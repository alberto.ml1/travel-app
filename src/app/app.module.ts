import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import {​​ HttpClientModule }​​ from '@angular/common/http';
import { PostUserComponent } from './pages/post-user/postUser-components/post-user.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PostUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
