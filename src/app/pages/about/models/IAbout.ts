import { TemplateAst } from '@angular/compiler';

export interface IAbout {
    titleAbout: string;
    descriptionAbout: string;
    card: Team;
}

export interface Team {
    imgProfile: string;
    iconLocation: string;
    profileName: string;
    location: string;
    description: string;
    iconFood: string;
    foodName: string;
    iconFilm: string;
    filmName: string;
    iconPower: string;
    powerName: string;
}



