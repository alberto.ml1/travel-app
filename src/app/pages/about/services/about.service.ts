import { ENDPOINTS } from './../../../endPoint/endpoints';
import { IAbout } from './../models/IAbout';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { getSyntheticPropertyName } from '@angular/compiler/src/render3/util';

@Injectable({
  providedIn: 'root',
})
export class AboutService {
  private aboutUrl: string = ENDPOINTS.about;

  constructor(private http: HttpClient) {}

  public getAbout(): Observable<any> {
    return this.http.get(this.aboutUrl).pipe(
      map((response: IAbout[]) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
