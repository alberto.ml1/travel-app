import { Component, OnInit } from '@angular/core';
import { IAbout } from '../models/IAbout';
import { AboutService } from './../services/about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public dataabout: IAbout[];

  constructor(private aboutService: AboutService) {}

  ngOnInit(): void {
    this.getAboutData();
  }

  public getAboutData(): void {
    this.aboutService.getAbout().subscribe(
      (data: IAbout[]) => {
        this.dataabout = data;
        console.log(this.dataabout);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}