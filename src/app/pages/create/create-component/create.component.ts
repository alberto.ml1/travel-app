import { PostUserComponent } from './../../post-user/postUser-components/post-user.component';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICreate, ITravel } from './../models/ICreate';
import { CreateService } from './../services/create.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  public signupForm: FormGroup;
  public datacreate: ICreate;
  public submitted: boolean = false;
  public userData: ITravel;

  constructor(
    private _builder: FormBuilder,
    private createService: CreateService
  ) {
    this.signupForm = this._builder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      destino: ['', Validators.required],
      presupuesto: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getCreateData();
  }
  public onSubmit(): void {
    this.submitted= true;
    if (this.signupForm.valid){
      
      this.postToUser();
      this.signupForm.reset();
      this.submitted= false;
    }
  }
  public postToUser(): void{
    
    const itravel: ITravel= {
      nombre: this.signupForm.get('nombre').value,
      apellido: this.signupForm.get('apellido').value,
      email: this.signupForm.get('email').value,
      destino: this.signupForm.get('destino').value,
      presupuesto: this.signupForm.get('presupuesto').value
    };
    this.createService.postDataUser(itravel).subscribe((data)=>{
      this.userData= data;
      console.log(this.userData);
      alert("Se han guardado tus datos en tu usuario")
    },
    (err)=>{console.error(err.message);}
    );
  }

  public getCreateData(): void {
    this.createService.getCreate().subscribe(
      (data: ICreate) => {
        this.datacreate = data;

        console.log(this.datacreate);
        
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
  
}
