export interface ICreate {

    imgContact: string;
    titleContact: string;

}
export interface ITravel{
    nombre: string;
    apellido: string;
    email: string;
    destino: string;
    presupuesto: string;
}
