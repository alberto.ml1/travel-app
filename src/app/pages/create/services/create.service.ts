import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ICreate, ITravel } from '../models/ICreate';
import { getSyntheticPropertyName } from '@angular/compiler/src/render3/util';

@Injectable({
  providedIn: 'root',
})
export class CreateService {
  private createUrl: string = 'http://localhost:3000/getContact';
  private userUrl: string = 'http://localhost:3000/dataUser';
  constructor(private http: HttpClient) {}

  public getCreate(): Observable<any> {
    return this.http.get(this.createUrl).pipe(
      map((response: ICreate) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
  public getUser(): Observable<ITravel> {
    return this.http.get(this.userUrl).pipe(
      map((response: ITravel) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public postDataUser(iTravel:ITravel):Observable<ITravel>{
    return this.http.post(this.userUrl, iTravel).pipe(
      map((response: ITravel) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
