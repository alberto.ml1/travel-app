import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { IHome } from '../models/IHome';
import { getSyntheticPropertyName } from '@angular/compiler/src/render3/util';
import { ENDPOINTS } from '../../../../app/endPoint/endpoints';


@Injectable({
  providedIn: 'root',
})
export class HomeService {
  private homeEndPoint: string = ENDPOINTS.home;

  constructor(private http: HttpClient) {}

  public getHome(): Observable<any> {
    return this.http.get(this.homeEndPoint).pipe(
      map((response: IHome) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
