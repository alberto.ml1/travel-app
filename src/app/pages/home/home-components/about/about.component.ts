import { Component, Input, OnInit } from '@angular/core';
import { IHomeNosotros } from '../../models/IHome';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  @Input() public dataHomeNosotros: IHomeNosotros;

  constructor() { }

  ngOnInit(): void {
  }

}
