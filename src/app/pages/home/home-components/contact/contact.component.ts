import { Component, Input, OnInit } from '@angular/core';
import { IHome, IHomeContacto } from '../../models/IHome';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  @Input() public dataHomeContacto: IHomeContacto;

  constructor() { }

  ngOnInit(): void {
  }

}
