import { Component, Input, OnInit } from '@angular/core';
import { IHomeAventura } from '../../models/IHome';

@Component({
  selector: 'app-adventures',
  templateUrl: './adventures.component.html',
  styleUrls: ['./adventures.component.scss']
})
export class AdventuresComponent implements OnInit {
@Input() public dataHomeAventura: IHomeAventura;
  constructor() { }

  ngOnInit(): void {
  }

}
