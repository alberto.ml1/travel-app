import { HomeService } from './../services/home.service';
import { Component, OnInit } from '@angular/core';
import {
  IHome,
  IHomeAventura,
  IHomeNosotros,
  IHomeContacto,
} from '../models/IHome';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public dataHome: IHome;
  public dataHomeAventura: IHomeAventura;
  public dataHomeNosotros: IHomeNosotros;
  public dataHomeContacto: IHomeContacto;

  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getHomeData();
  }

  public getHomeData(): void {
    this.homeService.getHome().subscribe(
      (data: IHome) => {
        this.mapHome (data);
        console.log('data:',data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
  public mapHome (data: IHome): void {
    this.dataHomeAventura = {
      imgPrincipal: data[0].imgPrincipal,
      titleAventura1: data[0].titleAventura1,
      titleAventura2: data[0].titleAventura2,
      descriptionAventura: data[0].descriptionAventura,
      iconAire: data[0].iconAire,
      copyAire: data[0].copyAire,
      iconAgua: data[0].iconAgua,
      copyAgua: data[0].copyAgua,
      iconFuego: data[0].iconFuego,
      copyFuego: data[0].copyFuego,
      iconTierra: data[0].iconTierra,
      copyTierra: data[0].copyTierra,
    };
    this.dataHomeNosotros = {
      titleNosotros: data[0].titleNosotros,
      titleNosotros2: data[0].titleNosotros2,
      descriptionNosotros: data[0].descriptionNosotros,
    };
    this.dataHomeContacto = {
      titleContacto1: data[0].titleContacto1,
      titleContacto2: data[0].titleContacto2,
    };
    console.log(this.dataHomeNosotros);
  }
}