import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-components/home.component';
import { AdventuresComponent } from './home-components/adventures/adventures.component';
import { AboutComponent } from './home-components/about/about.component';
import { ContactComponent } from './home-components/contact/contact.component';


@NgModule({
  declarations: [HomeComponent, AdventuresComponent, AboutComponent, ContactComponent ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
