export interface IHome {

    imgPrincipal: string;
    titleAventura1: string;
    titleAventura2: string;
    descriptionAventura: string;
    iconAire: string;
    copyAire: string;
    iconAgua: string;
    copyAgua: string;
    iconFuego: string;
    copyFuego: string;
    iconTierra: string;
    copyTierra: string;
    titleNosotros: string;
    titleNosotros2: string;
    descriptionNosotros: string;
    titleContacto1: string;
    titleContacto2: string;

}
export interface IHomeAventura {
    imgPrincipal: string;
    titleAventura1: string;
    titleAventura2: string;
    descriptionAventura: string;
    iconAire: string;
    copyAire: string;
    iconAgua: string;
    copyAgua: string;
    iconFuego: string;
    copyFuego: string;
    iconTierra: string;
    copyTierra: string;

}

export interface IHomeNosotros {
    titleNosotros: string;
    titleNosotros2: string;
    descriptionNosotros: string;

}
export interface IHomeContacto {
    titleContacto1: string;
    titleContacto2: string;

}