import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { IList } from '../list-component/models/IList';
import { getSyntheticPropertyName } from '@angular/compiler/src/render3/util';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private listUrl: string = 'http://localhost:3000/getListTravel';

  constructor(private http: HttpClient) {}

  public getList(): Observable<any> {
    return this.http.get(this.listUrl).pipe(
      map((response: IList) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}

