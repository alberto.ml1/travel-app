export interface IList {
    image: Img;
    icon: Img;
    country: string;
    title: string;
    days: number;
    price: number;

}

export interface Img {
    urlPackage: string;
    nameImg: string;
}

