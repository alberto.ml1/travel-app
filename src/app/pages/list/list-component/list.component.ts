import { IList } from './models/IList';
import { ListService } from './../services/list.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public datalist: IList;
  constructor(private listService: ListService) {}

  ngOnInit(): void {
    this.getListData();
  }

  public getListData(): void {
    this.listService.getList().subscribe(
      (data: IList) => {
        this.datalist = data;
        console.log(this.datalist);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}
