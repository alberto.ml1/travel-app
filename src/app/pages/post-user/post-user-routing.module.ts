import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostUserComponent } from './postUser-components/post-user.component';

const routes: Routes = [{
  path:'', component: PostUserComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostUserRoutingModule { }
