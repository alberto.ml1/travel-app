import { ITravel } from './../../create/models/ICreate';
import { CreateService } from './../../create/services/create.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-post-user',
  templateUrl: './post-user.component.html',
  styleUrls: ['./post-user.component.scss']
})
export class PostUserComponent implements OnInit {
  public user: ITravel;
  constructor(private createService: CreateService) { }

  ngOnInit(): void {
   this.getUser();
  }

  public getUser():void {
    this.createService.getUser().subscribe((data) => {
      this.user = data;
     console.log(this.user)
     console.log(data)
     
    });
  }
  
}
