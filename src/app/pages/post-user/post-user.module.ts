import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostUserRoutingModule } from './post-user-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PostUserRoutingModule
  ]
})
export class PostUserModule { }
