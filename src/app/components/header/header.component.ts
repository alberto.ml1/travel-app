import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public logo: string;

  constructor() { 
    this.logo = '../../../../assets/img/logo-hz-blanco.png';

    // const navUl = document.getElementById("#nav-ul");
    // const check = document.querySelector("#checkbox-mobile-menu") as HTMLInputElement;
  
    // navUl.addEventListener("click", ()=> {
    //   console.log(navUl);
    //   check.checked = !check.checked; 
    //  });
  }

  ngOnInit(): void {
  }


}